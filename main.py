from flask import Flask, render_template
from google.cloud import storage

app = Flask(__name__)


@app.route('/')
def hello_world():
    """List all files in GCP bucket."""
    client = storage.Client.from_service_account_json('./creds/tanitlabschool-dev-59f585263ad1.json')
    bucket_name = "traning_photos"
    bucket = client.bucket(bucket_name)
    files = bucket.list_blobs()
    url_images_list = []
    for file in files:
        if file._properties['name'].endswith(('jpeg', 'png', 'jpg', 'gif')):
            url_images_list.append(dict(
                url="https://storage.googleapis.com/traning_photos/" + file._properties['name'],
                name=file._properties['name'])
            )
    return render_template('display_images.html', title='display images', images_list=url_images_list)


if __name__ == '__main__':
    app.run(debug=True, port=5555)
